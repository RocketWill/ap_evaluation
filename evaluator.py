#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: willc
"""
import os
import json
import time
import tqdm
import numpy as np
from argparse import ArgumentParser

from data_loader import get_gt_boxes, get_predict_boxes
from calculate_ap import get_avg_precision_at_iou

def evaluate(gt_boxes, pred_boxes, output_path):
    start_time = time.time()
    avg_precs = []
    iou_thrs = []
    for idx, iou_thr in enumerate(tqdm.tqdm(np.linspace(0.5, 0.95, 10))):
        data = get_avg_precision_at_iou(gt_boxes, pred_boxes, iou_thr=iou_thr)
        avg_precs.append(data['avg_prec'])
        iou_thrs.append(iou_thr)

    avg_precs = [float('{:.4f}'.format(ap)) for ap in avg_precs]
    iou_thrs = [float('{:.4f}'.format(thr)) for thr in iou_thrs]
    print('mAP: {:.2f}'.format(np.mean(avg_precs)))
    print('avg precs: ', avg_precs)
    print('iou_thrs:  ', iou_thrs)
    end_time = time.time()
    print('\nCalculating mAP takes {:.4f} secs'.format(end_time - start_time))

    # get metric summary dict
    metric_summary = {}
    metric_summary['AP'] = round(np.mean(avg_precs), 4)
    for iou, avg_pre in zip(iou_thrs, avg_precs):
        metric_summary['AP' + str(int(iou*100))] = avg_pre

    # export result
    with open(output_path, 'w') as outfile:
        json.dump(metric_summary, outfile)
    print('Metric summary file is exported to {}'.format(output_path))

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("-gt", "--gt-folder", dest="gt", required=True, help="path to ground truth folder")
    parser.add_argument("-pred", "--pred-file", dest="pred", required=True, help="path to predict result file")
    parser.add_argument("-o", "--output", dest="output", required=False,
                        help="metric file output path, default is \"./metric_summary.json\"")
    args = parser.parse_args()

    gt_box = get_gt_boxes(args.gt)
    pred_boxes = get_predict_boxes(args.pred)
    output_path = args.output

    if not output_path:
        output_path = './metric_summary.json'
    else:
        os.makedirs(os.path.dirname(output_path), exist_ok=True)

    evaluate(gt_box, pred_boxes, output_path)