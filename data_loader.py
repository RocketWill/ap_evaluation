#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: willc
"""
import os
from collections import defaultdict
import json


def loadJsonFiles(dir_path):
    if os.path.isdir(dir_path):
        try:
            return [os.path.join(dir_path, f) for f in os.listdir(dir_path) if
                    os.path.join(dir_path, f).endswith('.json')]
        except:
            raise ValueError('Could not fetch json files in the path.')
    else:
        raise ValueError('Please enter a valid path.')

def _points2box(points):
    p1, p2 = points
    x1, y1 = p1
    x2, y2 = p2
    return [x1, y1, x2, y2]

def get_gt_boxes(gt_dir):
    ground_truth_boxes = defaultdict(list)
    labelme_files = loadJsonFiles(gt_dir)
    for file in labelme_files:
        try:
            with open(file, 'r') as reader:
                jf = json.loads(reader.read())
                shapes = jf['shapes']
                if len(shapes) < 1:
                    return
                img_name = os.path.basename(jf['imagePath'])
                boxes = []
                for shape in jf['shapes']:
                    if shape['label'] == 'ignore':
                        continue
                    else:
                        boxes.append(_points2box(shape['points']))
                ground_truth_boxes[img_name] = boxes
        except:
            print('Could not handle file {}. Skip over.'.format(file))
            continue
    return ground_truth_boxes

def get_predict_boxes(result_path):
    predict_boxes = defaultdict(dict)
    try:
        with open(result_path, 'r') as reader:
            for line in reader.readlines():
                line = line.strip()
                jf = json.loads(line)
                file_result = defaultdict(list)
                file_result.update({
                    'boxes': jf['bboxes'],
                    'scores': jf['scores']
                })
                predict_boxes.update({
                    jf['file_name']: file_result
                })
    except:
        print('Could not handle file {}. Skip over.'.format(result_path))
    return predict_boxes

if __name__ == "__main__":
    pass