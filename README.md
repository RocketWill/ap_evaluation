## Calculating single class bounding boxes Average Precision 
#### Requirements
- numpy
- tqdm

#### Help
```
usage: evaluator.py [-h] -gt GT -pred PRED [-o OUTPUT]

optional arguments:
  -h, --help            show this help message and exit
  -gt GT, --gt-folder GT
                        path to ground truth folder
  -pred PRED, --pred-file PRED
                        path to predict result file
  -o OUTPUT, --output OUTPUT
                        metric file output path, default is
                        "./metric_summary.json"

```

#### Usage
1. ```git clone git@gitlab.com:RocketWill/ap_evaluation.git```
2. ```cd ap_evaluation```
3. 
```bash
python evaluator.py -gt [folder contains GT files] -pred [result file] -o [metric file ouput path]
```

#### Description
1. GT files folder (json files)
- 为包含 labelme 标注文件的目录，目前仅支援 shape_type 为 rectangle 的 points 格式
- 以目录中单个标注文件为例：  
```json
{
  "version": "4.2.10",
  "flags": {},
  "shapes": [
    {
      "label": "rebar_bundle",
      "points": [
        [
          3503.4883720930234,
          1469.7674418604652
        ],
        [
          3544.186046511628,
          1503.4883720930234
        ]
      ],
      "group_id": null,
      "shape_type": "rectangle",
      "flags": {}
    },
    ...,
    ...,
  ],
  "imagePath":  "/path/of/image",
  "imageData":  "....",
  "imageHeight": 2736,
  "imageWidth": 3648
}
```
2. Predict Result file (json file)
- 为 predict 输出的结果
- result file 范例
```json
{"file_name":  'XXX.jpg', bboxes:  [[722.4339599609375, 1052.3311767578125, 1190.1181640625, 1451.99169921875], ...], scores:  [0.9999982118606567, ...]}
{"file_name":  'YYY.jpg', bboxes:  [[722.4339599609375, 1052.3311767578125, 1190.1181640625, 1451.99169921875], ...], scores:  [0.9999982118606567, ...]}
...
```

#### Output example
```json
{
  "AP": 0.3331, 
  "AP50": 0.6847, 
  "AP55": 0.6759, 
  "AP60": 0.58, 
  "AP65": 0.5392, 
  "AP70": 0.436, 
  "AP75": 0.2439, 
  "AP80": 0.1328, 
  "AP85": 0.0248, 
  "AP90": 0.0103, 
  "AP95": 0.0031,
}
```